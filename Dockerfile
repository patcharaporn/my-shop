FROM node:14-alpine

ENV NPM_CONFIG_LOGLEVEL warn
RUN yarn install

COPY / .

ENV NODE_ENV=production
RUN npm run build

EXPOSE 3001

CMD npm run start