
import React, { useState, useEffect, useRef, useLayoutEffect } from "react";
// node.js library that concatenates classes (strings)
import classnames from "classnames";
// reactstrap components
import {
  Card,
  CardHeader,
  Table,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
} from "reactstrap";

import UserHeader from "components/Headers/UserHeader.js";
import { getAllProduct } from "main-control/renderer";

const Sale = (props) => {
  const [total, setTotal] = useState(0);
  const [list, setList] = useState([
    { id: 1, product_name: "ข้าว", price: 20 },
    { id: 2, product_name: "น้ำ", price: 7 },
  ]);

  useEffect(() => {
    calTotal();
   
  });
const firstUpdate = useRef(true);
   useLayoutEffect(() => {
     if (firstUpdate.current) {
       firstUpdate.current = false;
       getProduct();
       return;
     }

     console.log("componentDidUpdateFunction");
   });

  const getProduct = () => {
    getAllProduct().then((res) => {
      console.log("getProduct " , res);
      if (res != null && res.status) {
        setList(res.result);
      } else {
      }
    });
  };

  const deleteList = (id) => {
    console.log('delete')
    var listgroup = [];
    for (var i = 0; i < list.length; i++) {
      console.log("list[i]", list[i]);
      console.log("id.id", id);
      if (list[i].id !== id) {
        listgroup.push(list[i]);
      }
    }
    
    setList(listgroup);
    console.log(list);
    calTotal();
  }

  const calTotal = () => {
     var cal = 0;
    list.map((data) => (cal = cal + data.price));
    setTotal(cal);
  }

  window.onkeypress = function () {
   
    calTotal();
    
    // alert("onkeypress");
  }

  return (
    <>
      <UserHeader />
      {/* Page content */}
      <Container fluid>
        <Form>
          <Row className="justify-content-md-center">
            <Col md="6">
              <FormGroup>
                <Input
                  className="form-control-alternative"
                  id="input-list"
                  placeholder="รายการ"
                  type="text"
                />
              </FormGroup>
            </Col>
          </Row>
        </Form>
        <Row className="justify-content-md-center mt-3">
          <Col md="8">
            <div className="col">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <h3 className="mb-0">รายการ</h3>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">ชื่อรายการ</th>
                      <th scope="col">ราคา</th>
                      <th scope="col" />
                    </tr>
                  </thead>
                  <tbody>
                    {list.map((data) => (
                      <tr key={data.id}>
                        <td>{data.product_name}</td>
                        <td>{data.price}</td>
                        <td
                          className="text-right"
                          onClick={() => deleteList(data.id)}
                        >
                          ลบ
                        </td>
                      </tr>
                    ))}
                    <tr>
                      <td> รวม </td>
                      <td colSpan="2" >
                        {total}
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </Card>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Sale;
