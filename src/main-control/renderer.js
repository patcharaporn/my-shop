// const { ipcRenderer, remote } = require("electron");
// const electron = window.require("electron");
// const { ipcRenderer } = electron;

export const send = (message) => {
  const electron = window.require("electron");
  const { ipcRenderer } = electron;

  return new Promise((resolve) => {
    ipcRenderer.once("asynchronous-reply", (_, arg) => {
      console.log("hi", arg);
      resolve(arg);
    });
    ipcRenderer.send("asynchronous-message", message);
  });
};

export const login = (data) => {
  var sha256 = require("js-sha256").sha256;
  if (!data) {
    return;
  }
    const electron = window.require("electron");
    const { ipcRenderer } = electron;
    data.password = sha256(data.password);
    return new Promise((resolve) => {
      console.log("response-login");
    ipcRenderer.once("response-login", (_, arg) => {
      console.log("response-login", arg);
      resolve(arg);
    });
    ipcRenderer.send("request-login", data);
  });
}

export const getAllProduct = () => {
  const electron = window.require("electron");
  const { ipcRenderer } = electron;
  return new Promise((resolve) => {
    console.log("response-get-all-product");
    ipcRenderer.once("response-get-all-product", (_, arg) => {
      console.log("response-get-all-product", arg);
      resolve(arg);
    });
    ipcRenderer.send("request-get-all-product");
  });
};

export default {
  send,
  login,
  getAllProduct,
};
