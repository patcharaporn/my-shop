const { ipcMain } = require("electron");
const sqlite3 = require("sqlite3");

const database = new sqlite3.Database("./public/myshop.db", (err) => {
  if (err) console.error("Database opening error: ", err);
});

ipcMain.on("asynchronous-message", (event, arg) => {
  const sql = arg;
  database.all(sql, (err, rows) => {
    event.reply("asynchronous-reply", (err && err.message) || rows);
  });
});

ipcMain.on("request-get-all-product", (event) => {
  console.log("request-get-all-product ==> ");
  database.all(
    "SELECT * FROM products",
    (err, result) => {
      var response = { result: {}, status: false };

      if (result != null) {
        response.result = result;
        response.status = true;
      }
      console.log("response : ", response);
      event.reply("response-get-all-product", (err && err.message) || response);
    }
  );
});

// ipcMain.on("request-login", (event, arg) => {
//   console.log("request-login ==> ", arg);
//   const sql = arg;
//   database.all(
//     "SELECT * FROM users WHERE username = (?) AND password = (?)",
//     sql.username,
//     sql.password,
//     (err, result) => {
//       var response = { username: '', status: false };
      
//       if (result != null && result.length > 0) {
//         response.username = result[0].username;
//         response.status = true;
//       }
//       console.log(result,"response : ", response);
//         event.reply("response-login", (err && err.message) || response);
//     }
//   );
// });

